// 场景：以前公司有gitlab仓库，现在仓库迁移，后续代码提交到新仓库
    // 我是clone了新仓库，把本地最新代码复制进去处理的

    // 现在可以使用 git config push.default current -> 是本地分支代码 会推送到远程（远程没有同名的分支 会创建一个）
    // git config push.default simple // 这是默认值 推到同名分支
    // git config push.default upstream // 推到上游分支



    // 测试一： 当前本地创建一个dev分支，修改git config时远程会自动创建一个dev分支吗
        // 测试通过： git config push.default current 如果远程没有同名分支，远程可以自动生成
    // 测试二：一个本地仓库 关联 两个远程仓库
        // 测试通过：
            // 前提条件：1. cat git config push.default -> 必须为current
            /*
                2. 本地有一个仓库，查看git remote -vv
                    origin 第一个仓库
                    执行命令：git remote set-url --add origin 新远程仓库地址
                    执行命令：git push 就好了

                    删除远程仓库： git remote rm 本地远程名

            */ 